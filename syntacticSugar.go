package main

import "fmt"

func main() {
	groupName := "catFishIsNotACat"
	groupSize := 6

	fmt.Println("We are the group", groupName, "and we have", groupSize, "members.")
}
