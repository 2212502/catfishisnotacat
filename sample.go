// this is a sample program that includes all topics demonstrated earlier in one program...
package main

// packages used in the program
import (
	"fmt"     //Package format implements formatted I/O
	"os"      // Package operating system provides a platform-independent interface to operating system functionality
	"reflect" // analyzes variables and values, and determines their type
	"sort"    // Package sort provides primitives for sorting slices and user-defined collections.
	"strconv" // Package string convert implements conversions to and from string representations of basic data types
)

func main() {

	// declare variables
	var input int
	var repeat bool
	var choice string

	for {
		greetUsers() // invoke the function.

		for { // user input validation.

			//input = checkInput()
			fmt.Print("\t Enter Choice: ")
			fmt.Scan(&choice)

			input = checkInput(choice)

			// if input is in between 1 to 3. It exits the loop & proceed to the switch statement.
			if input != 0 && input <= 3 && input > 0 {
				break
			} else { // limits the user input. If input is not in between 1 to 3 it breaks.
				fmt.Println("\nPlease enter numbers 1 to 3 only...\n")
			}

		}

		switch input { //SWITCH STATEMENTS
		case 1: // if input is 1 then, case 1 is executed.
			var userInput int
			fmt.Println("\n***************************************")
			fmt.Println("*               MASKIFY               *")
			fmt.Println("***************************************")
			fmt.Println("This will change all the number " +
				"you input into # except for the last four numbers...")
			fmt.Print("\tEnter the number:", " ")
			fmt.Scan(&userInput)
			maskify(strconv.Itoa(userInput))
			fmt.Println("\tResult: ", maskify(strconv.Itoa(userInput)))
			break

		case 2: // if input is 2 then, case 2 is executed.
			fmt.Println("\n***************************************")
			fmt.Println("*            SLICES & ARRAYS          *")
			fmt.Println("***************************************")
			fmt.Println("This will take 5 numbers then sort them in ascending order.")
			fmt.Println("\tEnter 5 numbers:", " ")
			var arr = make([]int, 5)

			for i := 1; i < 6; i++ {
				fmt.Print("(", i, "):", " ")
				fmt.Scan(&arr[i-1])
			}
			fmt.Println("The sorted numbers are: ", sortArray(arr))
			var start int
			var end int

			fmt.Println("Two numbers will be asked for the slice of an array.")

			fmt.Print("Enter start position: ")
			fmt.Scan(&start)

			fmt.Print("Enter end position: ")
			fmt.Scan(&end)
			fmt.Println("The sliced numbers are: ", getSlice(start, arr, end))

			break
		case 3: // if input is 3 then, case 3 is executed.
			fmt.Println("\nThank you for using our program, goodbye ヾ(^▽^*)\n")
			os.Exit(3)
			break
		}

		//invoke the function
		repeatProgram()

		for { // user input validation.

			//input = checkInput()
			fmt.Print("\t Enter Choice: ")
			fmt.Scan(&choice)

			input = checkInput(choice)

			// if input is in between 1 to 2. It exits the loop & proceed to the switch statement.
			if input != 0 && input <= 2 && input > 0 {
				break
			} else { // limits the user input. If input is not in between 1 to 2 it breaks.
				fmt.Println("\nPlease enter numbers 1 to 3 only...\n")
			}

		}

		// if input is 1 then, the program will be repeated.
		if input == 1 {
			repeat = true
		} else if input == 2 { // if input is 2 then, the program will be terminated.
			repeat = false
			fmt.Println("\nThank you for using our program, goodbye ヾ(^▽^*)\n")
			os.Exit(3)
		}

		if !(repeat) {
			break
		}
	}
}

// prints a welcome message and options of the program
func greetUsers() {
	programName := "catFishIsNotACAt"
	fmt.Println("\nWELCOME TO " + programName + " progarms")
	fmt.Println("***************************************")
	fmt.Println("* Enter [1] to MASK NUMBERS           *")
	fmt.Println("* Enter [2] Arrays and Slices         *")
	fmt.Println("* Enter [3] to EXIT                   *")
	fmt.Println("***************************************")
}

// prints a message whether the user wants to repeat the program or not
func repeatProgram() {
	fmt.Println("\nWould you like to try the program again?")
	fmt.Println("*********************")
	fmt.Println("* Enter [1] for YES *" +
		"\n* Enter [2] for NO  *")
	fmt.Println("*********************")
}

// This method will change all the number inputted into '#' except for the last four numbers
func maskify(input string) string {
	length := len(input)
	if length < 4 {
		return input
	} else {
		newInput := ""
		for i := 0; i < length; i++ {
			if i >= 4 {
				newInput += "#"
			}
		}
		newInput += input[len(input)-4 : len(input)]
		return newInput
	}
}

// Used to sort an array in ascending order
func sortArray(a []int) []int {
	sort.Ints(a)
	return a
}

// Used to return a slice from given array
func getSlice(n int, a []int, e int) []int {
	sort.Ints(a)
	slice := a[n : e-1]
	return slice
}

// checks and validates the input of the user
func checkInput(e string) int {
	var input int
	strTest, err := strconv.Atoi(e)

	if strTest == 0 {
		fmt.Println("")
		fmt.Print("Invalid Input. ", strTest, err, reflect.TypeOf(strTest), "\n")
	} else {
		input = strTest
	}
	return input
}
